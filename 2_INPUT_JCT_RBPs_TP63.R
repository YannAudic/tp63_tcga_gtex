# SCRIPT FOR INPUT OF JUNCTION_CENTRIC DATA FOR RBPs

# source("/home/yann/Documents/GTEX_NFYA/INPUT_JCT_RBPs_NFYA.R")
# OK RUN
# load data junctions for RBPs
library(tidyverse)
library(gridExtra)
#indir = "/mnt/BIODATA/GTEX_DATA_14122020"
		indir="/home/yann/Documents/GTEX_DATA_14122020"
		outdir="/home/yann/Documents/GTEX_TP63_22062022"
		if (!dir.exists(outdir)) {dir.create(outdir)}
# selection of RBP data from the big GTEX jct file
# get a list of ENSG for RBP grep -f based on this from the "GTEx_Analysis_2017-06-05_v8_STARv2.5.3a_junctions.gct" file
# save to subset_RBP.jct

# the column from the junction data are :
#	GTEX-PW2O-0008-SM-48TEB	GTEX-PW2O-0126-SM-48TC8
#	sed '3q;d' GTEx_Analysis_2017-06-05_v8_STARv2.5.3a_junctions.gct > RNASEQ_JUNCTION_samples.txt
#	sed 's/\t/\n/g' RNASEQ_JUNCTION_samples.txt > RNASEQ_junction_samples_column.txt
# I get a file with all the samples used in the junction analysis RNASEQ_junction_samples_column.txt
# based o nthe original GTEx_Analysis_2017-06-05_v8_STARv2.5.3a_junctions.gct file I obtain a vector of sample_name

# 								NORMALIZATION DATA 
# NORMALISATION OF THE DATA BASED ON SEQUENCING DEPTH
		normalisation_set =read.csv(file.path(indir,"GTEX_sample_junction"),sep="\t",header=FALSE)
		colnames(normalisation_set)=c("sample_ID","total_junc.count")
# need to format the sample_ID
		normalisation_set$sample_ID=gsub("-","_",normalisation_set$sample_ID)
		
#		normalisation_set <- normalisation_set %>% tibble() %>% filter(sample_ID %in% filtered_samples)

# I get a normalization factor for all the samples (17382)
# total_junct.count divided by the median of all the filtered samples
		normalisation_set <- normalisation_set %>% tibble()
		normalization_set <- normalisation_set %>% mutate(size_factor=total_junc.count / median(normalisation_set$total_junc.count))

# 									SAMPLE NAME/ID

		junction_samples=read.table(file.path(indir,"RNASEQ_junction_samples_column.txt"),skip=2,col.names="junction_samples")
# I convert the sample ID to have "_" as separator not "-"
		junction_samples[,1]= gsub("-","_",junction_samples[,1])
   		#annotation=junction_samples
		colnames_junction=c("junction_ID","ENSG_ID",as.character(junction_samples$junction_samples))


# 								RBP JUNCTION COUNT DATA

# preselected RBP junction file (this is subsetted outside of R)
		RBP_jct_file="subset_RBP.jct"
# analysis "RBP"
		#RBP_junction_gene_usage=read.table(file.path(indir,RBP_jct_file),col.names=colnames_junction)
		RBP_junction_gene_usage=read.table(file.path(indir,RBP_jct_file),col.names=colnames_junction)		
#  dim(RBP_junction_gene_usage)
# 7376 17384
# The annotation of these samples is : 	samples_junction_annotation=summarised_description[samples2analyse,]
# There is 17382 different sample_ID


# from there I can compute the sum of junction  for each sample and each ENSG_ID to obtain expression value (count) then normalize it based on the depth of expression (the proxy for that is the sum of all junction in each sample) computed from the original junction table.

# First pivot_longer then use the grouping variable to help to compute the desire value.
		RBP_JGU_long <- RBP_junction_gene_usage %>% tibble() %>%  pivot_longer(cols=starts_with("GTEX"), names_to = "sample_ID", values_to = "junction_count")
# A tibble: 128,209,632 x 4
#   junction_ID           ENSG_ID           sample_ID              junction_count
#   <fct>                 <fct>             <chr>                           <int>
# from there summarize by group with sum of junction

		RAW_RBP_jct_expression <- RBP_JGU_long %>% group_by(ENSG_ID,sample_ID) %>% summarise(junction_sum=sum(junction_count))

# OK
 # Now that I have this raw expression value for each sample I need to :
#########################################################################
# a) normalize it using the proxy for sequencing depth
#normalization_set
# A tibble: 1,753 x 3
#   sample_ID                total_junc.count size_factor
#   <chr>                               <int>       <dbl>
# 1 GTEX_1117F_0426_SM_5EGHI         13102176       0.841
# the value will be the normalized expression value for each ENSG in each sample
#########################################################################
		NORM_RBP_jct_expression <- RAW_RBP_jct_expression %>% full_join(y=normalization_set,by=c("sample_ID")) %>% mutate(Norm.junc.sum.exp=junction_sum/size_factor) 
# data for 386 RBPs(ENSG_ID) and 17382 samples

########################################################################
# GARBAGE CLEANING
# [1] "colnames_junction"         "description_file"         
# [3] "GENEX_JGU_long"            "GOI_jct"                  
# [5] "indir"                     "junction_samples"         
# [7] "NORM_GENEX_jct_expression" "NORM_RBP_jct_expression"  
# [9] "normalisation_set"         "normalization_set"        
#[11] "outdir"                    "RAW_RBP_jct_expression"   
#[13] "RBP_jct_file"              "RBP_JGU_long"             
#[15] "RBP_junction_gene_usage"   "rename"                   
#[17] "samples2analyse"           "simplified_description"   
#[19] "suffix"                    "summarised_description

gc()




# the objects to keep are 
#NORM_RBP_jct_expression
#GENEX_JGU_long


