# INPUT JCT DATA FROM GTEX FOR SPLICING ANALYSIS OF A SINGLE GENE "X"

# source("/home/yann/Documents/GTEX_NFYA/INPUT_JCT_NFYA_GENE.R")
# OK RUN
# load data junctions for RBPs
library(tidyverse)
library(gridExtra)
#indir = "/mnt/BIODATA/GTEX_DATA_14122020"
		indir="/home/yann/Documents/GTEX_DATA_14122020"
		outdir="/home/yann/Documents/GTEX_TP63_22062022"
		if (!dir.exists(outdir)) {dir.create(outdir)}
# selection of RBP data from the big GTEX jct file
# get a list of ENSG for RBP grep -f based on this from the "GTEx_Analysis_2017-06-05_v8_STARv2.5.3a_junctions.gct" file
# save to subset_RBP.jct

# the column from the junction data are :
#	GTEX-PW2O-0008-SM-48TEB	GTEX-PW2O-0126-SM-48TC8
#	sed '3q;d' GTEx_Analysis_2017-06-05_v8_STARv2.5.3a_junctions.gct > RNASEQ_JUNCTION_samples.txt
#	sed 's/\t/\n/g' RNASEQ_JUNCTION_samples.txt > RNASEQ_junction_samples_column.txt
# I get a file with all the samples used in the junction analysis RNASEQ_junction_samples_column.txt
# based o nthe original GTEx_Analysis_2017-06-05_v8_STARv2.5.3a_junctions.gct file I obtain a vector of sample_name


# 								NORMALIZATION DATA 
# NORMALISATION OF THE DATA BASED ON SEQUENCING DEPTH
		normalisation_set =read.csv(file.path(indir,"GTEX_sample_junction"),sep="\t",header=FALSE)
		colnames(normalisation_set)=c("sample_ID","total_junc.count")
# need to format the sample_ID
		normalisation_set$sample_ID=gsub("-","_",normalisation_set$sample_ID)
#		normalisation_set <- normalisation_set %>% tibble() %>% filter(sample_ID %in% filtered_samples)
# I get a normalization factor for all the samples (17382)
# total_junct.count divided by the median of all the filtered samples
		normalisation_set <- normalisation_set %>% tibble()
		normalization_set <- normalisation_set %>% mutate(size_factor=total_junc.count / median(normalisation_set$total_junc.count))

# 									SAMPLE NAME/ID
		junction_samples=read.table(file.path(indir,"RNASEQ_junction_samples_column.txt"),skip=2,col.names="junction_samples")
# I convert the sample ID to have "_" as separator not "-"
		junction_samples[,1]= gsub("-","_",junction_samples[,1])
   		#annotation=junction_samples
		colnames_junction=c("junction_ID","ENSG_ID",as.character(junction_samples$junction_samples))


# 					 A DESCRIPTION of samples  IS AVAILABLE HERE 
#   https://www.ebi.ac.uk/arrayexpress/files/E-MTAB-5214/E-MTAB-5214.sdrf.txt
		description_file="E-MTAB-5214.sdrf.txt"
		description_table=read.csv(file.path(indir,description_file),sep="\t",header=TRUE)
		description_table <- tibble(description_table)
		# NEED TO REFORMAT Source.Name
		rename=gsub("-","_",as.character(description_table$Source.Name))
		description_table$Source.Name=rename
		# 53 different tissue part for a total number of 37742 lines corresponding to 37742 sequencing dataset.

# There is a total number of 9668 different samples 
# length(unique(description_table$Source.Name)) #9668
# FROM 550 different patients 
# length(unique(description_table$Characteristics.individual.))
# How many male and female among the 550 patients ?
# Characteristics.individual.
# Characteristics.sex.
		description_table %>% filter(Characteristics.sex.=="female") %>% group_by(Characteristics.individual.) # 189 female
		description_table %>% filter(Characteristics.sex.=="male") %>% group_by(Characteristics.individual.) # 361 males
# OK sum up at 550 patients
# What I want is a) to generate a simple annotation table containing
#		SAMPLEID PATIENTID TISSUE_TYPE
		simplified_description <-	description_table %>% select("Source.Name","Characteristics.individual.","Characteristics.sex.","Comment.original.body.site.annotation.")
		summarised_description <- simplified_description %>% distinct()
# I convert the "-" to "_"
		rename=gsub("-","_",as.character(summarised_description$Source.Name))
		summarised_description$Source.Name=rename


# 						INPUT JCT DATA FOR A GIVEN GENE
# the data are first selected from outside R to get only data corresponding to the ENSG_ID of interest
		GOI_jct="TP63"
		suffix="junctions.gct"
		junction_gene_usage=read.table(file.path(indir,paste(GOI_jct,suffix,sep="_")),col.names=colnames_junction)
		
# create a table that will contain the junction  data
		GENEX_JGU_long <- junction_gene_usage %>%  tibble() %>% pivot_longer(cols=starts_with("GTEX"), names_to = "sample_ID", values_to = "junction_count")

# compute normalized junction usage for each junction and each sample and add it to the object
		GENEX_JGU_long <-GENEX_JGU_long  %>% full_join(y=normalization_set,by=c("sample_ID")) %>% mutate(Norm.junc=junction_count/size_factor)

# compute jct based expression and normalize for each samples
		RAW_GENEX_jct_expression <- GENEX_JGU_long %>% group_by(ENSG_ID,sample_ID) %>% summarise(junction_sum=sum(junction_count))

# I need to compute a normalized junction usage that is the junction count divided by the sum of all the junction for GENEX in the sample studied. This allows to eliminate variation on genex expression and focus on variation of junction usage. jct_fm=junction_count(i) / SUM(junction_count) for a sample
		GENEX_JGU_long %>% group_by(sample_ID) %>% mutate (jct_fm=junction_count/(sum(junction_count))) -> GENEX_JGU_long

# before grouping by samples I am going to compute a "junction_normalized" EXPRESSION to take into account the expression level (I can not normalize over the depth of sequencing As I use only gene by gene info

		NORM_GENEX_jct_expression <- RAW_GENEX_jct_expression %>% full_join(y=normalization_set,by=c("sample_ID")) %>% mutate(Norm.junc.sum.exp=junction_sum/size_factor) 
		
########################################################################		
	length(intersect(junction_samples$junction_samples,as.character(summarised_description$Source.Name))) # 8675
# 8675 samples have junction count informations
	samples2analyse=intersect(junction_samples$junction_samples,as.character(summarised_description$Source.Name))
	samples2analyse=gsub("-","_",samples2analyse)
	

########################################################################
# GARBAGE CLEANING	

# [1] "colnames_junction"         "description_file"         
# [3] "description_table"         "GENEX_JGU_long"           
# [5] "GOI_jct"                   "indir"                    
# [7] "junction_gene_usage"       "junction_samples"         
# [9] "NORM_GENEX_jct_expression" "normalisation_set"        
#[11] "normalization_set"         "outdir"                   
#[13] "RAW_GENEX_jct_expression"  "rename"                   
#[15] "samples2analyse"           "simplified_description"   
#[17] "suffix"                    "summarised_description"

# the objects to keep are 
# 	NORM_GENEX_jct_expression
#	GENEX_JGU_long	
# 	samples2analyse

# the objects I can remove are 
# junction_gene_usage
# description_table
# RAW_GENEX_jct_expression
# 
rm(junction_gene_usage,description_table,RAW_GENEX_jct_expression,normalisation_set)
gc()
