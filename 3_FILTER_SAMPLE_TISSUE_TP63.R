# FOR A GENE OF INTEREST SELECT TISSUE WHERE IT IS EXPRESSED
# DEFINE A THRESHOLD OF EXPRESSION

#source("/home/yann/Documents/GTEX_NFYA/FILTER_SAMPLE_TISSUE_NFYA.R")

# will use script INPUT_JCT_GENEX
#outdir="FIGURES_12012020"

# the description table contains info for 37742 samples
# description_table -> summarised_description
	summarised_description$Source.Name
# junction_samples # samples for which junction data are available I have 17382 junction samples
	samples2analyse=intersect(junction_samples$junction_samples,as.character(summarised_description$Source.Name))

# AND In fact only 8675 samples with junction data and sample description.
# Based on this list of sample I can simplify the dataset to keep only the infos for samples2analyse.

	NORM_RBP_jct_expression %>% filter(sample_ID %in% samples2analyse) 
# from  6,709,452 x 6 TO 3,348,550 x 6

#GENEX expression based on jct data
	NORM_GENEX_jct_expression %>% filter(sample_ID %in% samples2analyse) 
# from  17,382 x 6 TO 8,675 x 6

# GENEX JUNCTION USAGE
	GENEX_JGU_long # jct_count_data for a given gene in all samples [ENSGID is unique] NOT NORMLALIZED BY SIZE FACTOR
# from 365,022 x 4 to 182,175 x 4
	GENEX_JGU_long %>% filter(sample_ID %in% samples2analyse) ## A tibble: 86,750 x 8

# FONCTION POUR RECUPERER LES TISSUS OU LE GENEX est exprimé.
# the annotation pertaining to the sample to analyse is 
	summarised_description %>% filter(Source.Name %in% samples2analyse) -> selected_annotation
# from 9,668 x 4 TO 8,675 x 4
# FOR THE WHOLE ANNOTATION # caution, some technical infos have been dropped 

# I will annotate the EXPRESSION DATA
#‘left_join()’: includes all rows in ‘x’.
	NORM_GENEX_jct_expression %>% filter(sample_ID %in% samples2analyse) %>% left_join(y=selected_annotation,by=c("sample_ID" = "Source.Name"))
# A tibble: 8,675 x 9
# BASED ON THIS TABLE PLOT EXPRESSION BY TISSUE
	NORM_GENEX_jct_expression %>% filter(sample_ID %in% samples2analyse) %>% left_join(y=selected_annotation,by=c("sample_ID" = "Source.Name")) -> NORM_GENEX_jct_expression_annotated



# PLOT OF RAW EXPRESSION OF GENEX FOR EACH SAMPLE GROUPED BY TISSUE TYPE
	NORM_GENEX_jct_expression_annotated %>% ggplot() + geom_jitter(aes(x= Comment.original.body.site.annotation.,y=junction_sum,group= Comment.original.body.site.annotation.,color= Comment.original.body.site.annotation.)) -> plotRAWexp_GENEX_bytissue

# PLOT OF NORMALISED EXPRESSION OF GENEX FOR EACH SAMPLE GROUPED BY TISSUE TYPE
	NORM_GENEX_jct_expression_annotated %>% ggplot() + geom_jitter(aes(x= Comment.original.body.site.annotation.,y=Norm.junc.sum.exp,group= Comment.original.body.site.annotation.,color= Comment.original.body.site.annotation.)) -> plotNORMexp_GENEX_bytissue
	
# too many tissue type where expression is absent.

# therefore I focus on the samples where there is expression and I defined this based on the median TP63 junction_sum across tissue type

	NORM_GENEX_jct_expression_annotated %>%group_by(Comment.original.body.site.annotation.) %>% summarise(median=median(junction_sum)) -> summary_GENEX
	# this tibble has all tissues with the median expression value (normalized junction sum) 
	
# A tibble: 53 x 2
# WE CAN THEREFORE SELECT TISSUES AS PERTINENT FOR SPLICING ANALYSIS
# tissue for which the median junction count is >= 100
	summary_GENEX %>% filter(median >=100) %>% pull(Comment.original.body.site.annotation.) -> 	pertinent_GENEX_tissue
	
# OK I SELECT DATA FROM PERTINENT TISSUES AND with sufficient expression value in the sample (50 is twice the number of splice junction)
	NORM_GENEX_jct_expression_annotated %>% filter(Comment.original.body.site.annotation. %in% pertinent_GENEX_tissue) %>% filter(junction_sum >50)	
		
# then we select the samples from the pertinent tissues whatever is their expression level.	have a look at the expression
	NORM_GENEX_jct_expression_annotated %>% filter(Comment.original.body.site.annotation. %in% pertinent_GENEX_tissue) %>% filter(junction_sum >50) %>% ggplot() + geom_jitter(aes(x= Comment.original.body.site.annotation.,y=Norm.junc.sum.exp,group= Comment.original.body.site.annotation.,color= Comment.original.body.site.annotation.)) -> plot_NORMTP63_expression_selected_tissue


#######################pertinent_GENEX_tissue###########################
		pertinent_GENEX_tissue
########################################################################
# ANALYSIS OF SPLICING ACROSS THE DIFFERENT TISSUE TYPES with samples filtered for abundance of junction and pertinence of the tissue

GENEX_JGU_long %>% left_join(y=selected_annotation,by=c("sample_ID" = "Source.Name")) %>% filter(sample_ID %in% samples2analyse) %>% filter(Comment.original.body.site.annotation. %in% pertinent_GENEX_tissue) %>% group_by(sample_ID) %>% filter (sum(junction_count)> 50) -> FILTERED_GENEX_JGU_long


# using RAW "junction_count" OR sizefactor normalized "Norm_jct" OR "jct_fm"

plot_junction_count <- FILTERED_GENEX_JGU_long %>% ggplot(aes(x=Comment.original.body.site.annotation.,y=100* junction_count)) + geom_jitter(aes(color=Comment.original.body.site.annotation.),cex=0.5) + facet_wrap(~junction_ID)

plot_Norm_jct <- FILTERED_GENEX_JGU_long %>% ggplot(aes(x=Comment.original.body.site.annotation.,y=100* Norm.junc)) + geom_jitter(aes(color=Comment.original.body.site.annotation.),cex=0.5) + facet_wrap(~junction_ID)

plot_jct_fm <- FILTERED_GENEX_JGU_long %>% ggplot(aes(x=Comment.original.body.site.annotation.,y=100* jct_fm)) + geom_jitter(aes(color=Comment.original.body.site.annotation.),cex=0.5) + facet_wrap(~junction_ID)


		# junction_count
		#plot_junction_count <- GENEX_JGU_long %>% left_join(y=selected_annotation,by=c("sample_ID" = "Source.Name")) %>% filter(sample_ID %in% samples2analyse) %>% filter(Comment.original.body.site.annotation. %in% pertinent_GENEX_tissue) %>% ggplot(aes(x=Comment.original.body.site.annotation.,y=100* junction_count)) + geom_jitter(aes(color=Comment.original.body.site.annotation.)) + facet_wrap(~junction_ID)
	
		# Norm_jct
		#plot_Norm_jct <- GENEX_JGU_long %>% left_join(y=selected_annotation,by=c("sample_ID" = "Source.Name")) %>% filter(sample_ID %in% samples2analyse) %>% filter(Comment.original.body.site.annotation. %in% pertinent_GENEX_tissue) %>% ggplot(aes(x=Comment.original.body.site.annotation.,y=100* Norm.junc)) + geom_jitter(aes(color=Comment.original.body.site.annotation.)) + facet_wrap(~junction_ID)	
		
		#jct_fm	
		#plot_jct_fm <- GENEX_JGU_long %>% left_join(y=selected_annotation,by=c("sample_ID" = "Source.Name")) %>% filter(sample_ID %in% samples2analyse) %>% filter(Comment.original.body.site.annotation. %in% pertinent_GENEX_tissue) %>% ggplot(aes(x=Comment.original.body.site.annotation.,y=100* jct_fm)) + geom_jitter(aes(color=Comment.original.body.site.annotation.)) + facet_wrap(~junction_ID)	

################# MAKE A FIGURE ########################################

	#pdf(file.path(outdir,"DNAJC2_junct.pdf"), width=11.69, height=11.02)	
		
	# gene expression by tissue 
		#plotRAWexp_GENEX_bytissue + theme(axis.text.x = element_text(angle = 90, hjust = 1),legend.position = "none")
		#plotNORMexp_GENEX_bytissue + theme(axis.text.x = element_text(angle = 90, hjust = 1),legend.position = "none")
		
	# gene expression by tissue for pertinent tissues and samples with sufficient data (junction_sum >50)
	#	plot_NORMTP63_expression_selected_tissue + theme(axis.text.x = element_text(angle = 90, hjust = 1),legend.position = "none")
	
	
	# junction usage
		#plot_junction_count + theme(axis.text.x = element_text(angle = 90, hjust = 1),legend.position = "none")
		#plot_Norm_jct + theme(axis.text.x = element_text(angle = 90, hjust = 1),legend.position = "none")
	#	plot_jct_fm + theme(axis.text.x = element_text(angle = 90, hjust = 1),legend.position = "none")
	#	dev.off()
		
########################################################################
# plot only junction usage 	for supplmentary figure	
#		png(filename =file.path(indir,outdir,"TP63_junct_FM.png"), width = 210, height = 210, units = "mm", pointsize = 12,bg ="white",  res = 300, type ="cairo-png")
#		plot_jct_fm + xlab("Tissue")+  theme(legend.position = "bottom", strip.text = element_text(size=7)) +  scale_x_discrete(labels=c(1:10))
#		dev.off()

########################################################################
# Plotting junction usage / tissues for junctions :
#TP63
jct2plot=c("chr3_189872996_189880063","chr3_189872996_189886393","chr3_189889485_189890788","chr3_189889485_189894205","chr3_189890883_189894205")
# STAT3 jct2plot=c("chr17_42316852_42317181","chr17_42316902_42317181")

	#Calculating 
		pdf(file.path(outdir,"TP63_selected_junct.pdf"), width=11.69, height=11.02)
		plot_selected_junction <- FILTERED_GENEX_JGU_long %>% filter(junction_ID %in% jct2plot) %>% ggplot(aes(x=Comment.original.body.site.annotation.,y=100* jct_fm)) + geom_jitter(aes(color=Comment.original.body.site.annotation.),cex=0.5) + facet_wrap(~junction_ID)
		N_labels=length(unique(FILTERED_GENEX_JGU_long$Comment.original.body.site.annotation.))
		plot_selected_junction + xlab("Tissue")+  theme(legend.position = "bottom", strip.text = element_text(size=7)) +  scale_x_discrete(labels=c(1:N_labels))
		dev.off()


# need to clean the memory a little bit to have things running later on
# need to keep
#       NORM_GENEX_jct_expression_annotated
#		FILTERED_GENEX_JGU_long
# 		jct2plot
# 		pertinent_GENEX_tissue
# 		indir,outdir

objects2KEEP=c("jct2plot","FILTERED_GENEX_JGU_long","indir","NORM_RBP_jct_expression","outdir","selected_annotation","pertinent_GENEX_tissue")
rm(list=ls()[which(FALSE== (ls() %in% objects2KEEP))])
gc()

#> objects()
# [1] "colnames_junction"                       
# [2] "description_file"                        
# [3] "FILTERED_GENEX_JGU_long"      ###################################
# [4] "GENEX_JGU_long"                          
# [5] "GOI_jct"                                 
# [6] "indir"                         ################################## 				                           
# [7] "jct2plot"                                
# [8] "junction_samples"                        
# [9] "NORM_GENEX_jct_expression"               
#[10] "NORM_GENEX_jct_expression_annotated"     
#[11] "NORM_RBP_jct_expression"             ############################           
#[12] "normalisation_set"                       
#[13] "normalization_set"                       
#[14] "outdir"              			    ############################        	              
#[15] "pertinent_GENEX_tissue"                  
#[16] "plot_jct_fm"                             
#[17] "plot_junction_count"                     
#[18] "plot_Norm_jct"                           
#[19] "plot_NORMTP63_expression_selected_tissue"
#[20] "plot_selected_junction"                  
#[21] "plotNORMexp_GENEX_bytissue"              
#[22] "plotRAWexp_GENEX_bytissue"               
#[23] "RAW_RBP_jct_expression"                  
#[24] "RBP_jct_file"                            
#[25] "RBP_JGU_long"                            
#[26] "RBP_junction_gene_usage"                 
#[27] "rename"                                  
#[28] "samples2analyse"                         
#[29] "selected_annotation"                     
#[30] "simplified_description"                  
#[31] "suffix"                                  
#[32] "summarised_description"                  
#[33] "summary_GENEX" 
	
	
	
	
	
	
	
	
	
	


