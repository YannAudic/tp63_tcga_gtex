# TP63/PTBP1 manuscript gitlab for R script

This project host the R scripts associated to the manuscript Taylor et al.

1) Analysis of RNA splicing for TP63 gene in TCGA HNSCC data and survival analysis
"TCGA_TP63_taylor01092021.R"

2) Analysis of electrophoretic mobility shift assays
"analyse_gelshift_tidyggplot.R"


3) Analysis of RBPs/splicing (junction usage) correlation in GTEX data.
The 5 script noted N_xxx.R should be run sequentially.




